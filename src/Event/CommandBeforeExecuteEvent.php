<?php

namespace Soen\Command\Event;

use Soen\Event\EventInterface;

/**
 * Class CommandBeforeExecuteEvent
 * @package Soen\Command\Event
 */
class CommandBeforeExecuteEvent implements EventInterface
{

    /**
     * @var string
     */
    public $command;

    /**
     * CommandBeforeExecuteEvent constructor.
     * @param string $command
     */
    public function __construct(string $command)
    {
        $this->command = $command;
    }

}
