<?php


namespace Soen\Command;


use App\Command\HttpServerCommand;
use Soen\Filesystem\File;
use Symfony\Component\Console\Application;

class CommandProvider
{
	public $appConsole;
	public $commands = [];
	function __construct()
	{
		$this->appConsole = new Application();
		foreach ($this->commands as $command){
            $command = str_replace('\\\\', '\\',$command);
			$this->appConsole->add(new $command);
		}
//		$this->appConsole->addCommands($this->commands);
		$this->appConsole->run();
	}
}